<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dev Test</title>

    <link href="assets/bootstrap5/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="container">
            </br>
            <h2>{{ title }}</h2>
            </br>
            <?php 
                $timestamp = strtotime('next Monday');
                $days = array();
                for ($i = 0; $i < 7; $i++) {
                    $days[] = strftime('%A', $timestamp);
                    $timestamp = strtotime('+1 day', $timestamp);
                }

                $today = date('l');
            ?>
            <div class="row" style="background-color: #ededed; height: 50px; margin-left: 200px;margin-right: 200px;">
                <div class="col-md-12" style="margin-top: 15px;">
                    <h6 style="text-align: center;">Today Is <span style="color: green;"><?=$today;?></span></h6>
                </div>  
            </div>
            <div class="row" style="margin-left: 200px;margin-right: 200px;">
                <div class="col-md-12" style="background-color: #cbcbcb; height: 60px;">
                    <nav class="navbar navbar-light" style="background-color: #cbcbcb; height: 60px; margin-left: 0px;margin-right: 0px;">
                        <div class="container-fluid">
                            <?php foreach ($days as $key => $value): ?>
                                <?php if($today != $value): ?>
                                    <button type="button" class="btn btn-primary" @click="select_day('<?=$value;?>')" :style="todayColor('<?=$value;?>')"><?=$value;?></button>
                                <?php else: ?>
                                    <button type="button" class="btn btn-primary" @click="select_day('<?=$value;?>')" :style="todayColor('<?=$value;?>')"><?=$value;?></button>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </nav>
                </div>
            </div>
            <div class="row" style="background-color: #ededed; margin-left: 200px;margin-right: 200px;">
                <div class="col-md-12" style="margin-top: 6px;">
                    <h6 style="text-align: center;">Selected Day Is <span style="color: #0094f8;">{{ selected_day }}</span></h6>
                </div>  
            </div>
            </br>
        </div>
    </div>
</body>

<script src="assets/bootstrap5/js/bootstrap.bundle.min.js"></script>
<script src="assets/moment.js"></script>
<script src="assets/vueJS/vue.min.js"></script>
<script>
    new Vue({
        el: '#app',
        data : {
            title: "Developer Test Phase 1",
            day_range: [
                { day_value: "Monday" },
                { day_value: "Tuesday" },
                { day_value: "Wednesday" },
                { day_value: "Thursday" },
                { day_value: "Friday" },
                { day_value: "Saturday" },
                { day_value: "Sunday" },
            ],
            today: "",
            selected_day: "",
            outline: ""
        },
        created() {
            this.getToday();
        },
        methods: {
            select_day(day) {
                this.outline = '';
                this.selected_day = day;
            },
            getToday() {
                var date = new Date();
                this.today = moment(date).format('dddd');
                this.selected_day = this.today;
            },
            todayColor(day) {
                if (this.today == day) {
                    
                    if (this.selected_day == day) {
                        return 'background-color: green; border-radius: 0px; margin-top: 2px; outline: groove; outline-color: white; outline-width: 2px;';
                    } else {
                        return 'background-color: green; border-radius: 0px; margin-top: 2px';
                    }
                    
                } else {

                    if (this.selected_day == day) {
                        return 'background-color: #0094f8; border-radius: 0px; margin-top: 2px; outline: groove; outline-color: white; outline-width: 2px;';
                    } else {
                        return 'background-color: #0094f8; border-radius: 0px; margin-top: 2px;';
                    }
                }  
            }
        },
        computed: {

        }
    })
</script>


</html>

<!-- outline: groove;
outline-color: white;
outline-width: 6px; -->